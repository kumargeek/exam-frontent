"use strict";
var question = angular.module("apptha.question", []);
question.directive("notification", notificationDirective);
question.factory("requestFactory", requestFactory);
question.directive("baseValidator", validatorDirective);
question.factory("requestFactory", requestFactory);

question.controller("QuestionController", ["$window", "$scope", "requestFactory","$http","$rootScope", function($window, scope, requestFactory, $http,$rootScope) {
    requestFactory.setThisArgument(this);
    var self = this;
    this.question = {};
    this.categories = {};
    this.tags = {};
    scope.errors = {};
    this.options = {};
    this.questionAlignment = [{
            'id': 1,
            'name': 'Left'
        },
        {
            'id': 2,
            'name': 'Right'
        },
        {
            'id': 3,
            'name': 'Top'
        },
        {
            'id': 4,
            'name': 'Bottom'
        },{
            'id': 5,
            'name': 'Center'
        }
    ];
    
    this.questionOptAlignment = [{
        'id': 1,
        'name': 'Left'
    },
    {
        'id': 2,
        'name': 'Right'
    },
    {
        'id': 3,
        'name': 'Top'
    },
    {
        'id': 4,
        'name': 'Bottom'
    },{
        'id': 5,
        'name': 'Center'
    }
];
    
    this.questionAnswers = [{
        'id': 1,
        'name': 'A'
    },
    {
        'id': 2,
        'name': 'B'
    },
    {
        'id': 3,
        'name': 'C'
    },
    {
        'id': 4,
        'name': 'D'
    },{
        'id': 5,
        'name': 'Others'
    }
];
 this.statusDropDown = [{
	 'key' : 1,
	 'value': 'Active'
 }, {
     'key' : 0,
     'value' : 'Inactive'
 }];
    
    
    
    this.questionType = [{
        'id': 1,
        'name': 'Fill in the blank'
    },
    {
        'id': 2,
        'name': 'Choose the best answer'
    }];

    /**
     * Method to get the rules for the add form
     */
    this.fetchRules = function() {
        requestFactory.get(requestFactory.getUrl("question/create"), function(response) {
        	baseValidator.setRules(response.rules);
        	this.categories = response.categories;
        	this.tags = response.tags;
        }, function() {});
    };
    /**
     * Method to save the add form data
     */
    this.save = function(event) {
    	
    	if (baseValidator.validateAngularForm(event.target, scope)) {
            this.question.id = this.id
            this.question.user_id = 1;
            this.question.is_active = 1;
            
            
            console.log(this.question);
            debugger;
            
            requestFactory.post(requestFactory.getUrl("question"), this.question, function(response) {
                $window.location = requestFactory.getTemplateUrl("question");
                $rootScope.notification.add({isSuccess : true,message : response.message}).showLater();
            }, this.fillError);
        }
    };
    
    scope.fileUpload=function(file,scopeModel){
  	
  	  var fd = new FormData();
        fd.append('question_image', file);
        
        $http.post(requestFactory.getUrl('questionfileupload'), fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	if(data.error==false){
      		  self.question.question_image=data.message;
      		  
      	  }
        })
        .error(function(data){
      	 
        });
    };
    
    scope.fileUploada=function(file,scopeModel){
      	
    	  var fd = new FormData();
          fd.append('option_a_img', file);
          
          $http.post(requestFactory.getUrl('questionfileuploada'), fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
          })
          .success(function(data){
          	if(data.error==false){
          		
          		console.log(data.message);
          		debugger;
          		
        		  self.question.option_a_img=data.message;
        		  
        	  }
          })
          .error(function(data){
        	 
          });
      };
      
      scope.fileUploadb=function(file,scopeModel){
    	  	
      	  var fd = new FormData();
            fd.append('option_b_img', file);
            
            $http.post(requestFactory.getUrl('questionfileuploadb'), fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
            .success(function(data){
            	if(data.error==false){
          		  self.question.option_b_img=data.message;
          		  
          	  }
            })
            .error(function(data){
          	 
            });
        };
        
        scope.fileUploadc=function(file,scopeModel){
          	
        	  var fd = new FormData();
              fd.append('option_c_img', file);
              
              $http.post(requestFactory.getUrl('questionfileuploadc'), fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
              })
              .success(function(data){
              	if(data.error==false){
            		  self.question.option_c_img=data.message;
            		  
            	  }
              })
              .error(function(data){
            	 
              });
          };
          
          scope.fileUploadd=function(file,scopeModel){
        	  	
          	  var fd = new FormData();
                fd.append('option_d_img', file);
                
                $http.post(requestFactory.getUrl('questionfileuploadd'), fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                .success(function(data){
                	if(data.error==false){
              		  self.question.option_d_img=data.message;
              		  
              	  }
                })
                .error(function(data){
              	 
                });
            };
    
    
    /**
     * Method to handle the error message.
     */
    this.fillError = function(response) {
        this.isDisabled = false;
        if (response.status == 422 && response.data.hasOwnProperty("messages")) {
            angular.forEach(response.data.messages, function(message, key) {
                if (typeof message == "object" && message.length > 0) {
                    scope.errors[key] = {
                        has: true,
                        message: message[0]
                    };
                }
            });
        }
    };
    /**
     * Method to get the single record data for the zones
     *
     * @param id, record id
     * @returns object
     */
    this.fetchSingleInfo = function(id) {
        this.id = id;
        requestFactory.get(requestFactory.getUrl("question/editquestion/" + this.id), function(response) {
        	this.question = response.data.questionSingleInfo;
        	this.categories = response.data.categories;
        	this.tags = response.data.tags;
        	this.options = response.data.options;
        	
        	this.question.option_a = this.options.option_a;
        	this.question.option_a_img = this.options.option_a_img;
        	this.question.option_a_alignment = this.options.option_a_alignment;
        	
        	this.question.option_b = this.options.option_b;
        	this.question.option_b_img = this.options.option_b_img;
        	this.question.option_b_alignment = this.options.option_b_alignment;
        	
        	this.question.option_c = this.options.option_c;
        	this.question.option_c_img = this.options.option_c_img;
        	this.question.option_c_alignment = this.options.option_c_alignment;
        	
        	this.question.option_d = this.options.option_d;
        	this.question.option_d_img = this.options.option_d_img;
        	this.question.option_d_alignment = this.options.option_d_alignment;
        	
        	baseValidator.setRules(response.rules);
        }, function() {});
    };
}]).directive('fileModel', ['$parse', function ($parse,$http) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
        	var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
            	scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                    scope.fileUpload( element[0].files[0],attrs.fileModel)
                });
            });
        }
    };
}]).directive('fileModela', ['$parse', function ($parse,$http) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
        	var model = $parse(attrs.fileModela);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
            	scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                    scope.fileUploada( element[0].files[0],attrs.fileModela)
                });
            });
        }
    };
}]).directive('fileModelb', ['$parse', function ($parse,$http) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
        	var model = $parse(attrs.fileModelb);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
            	scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                    scope.fileUploadb( element[0].files[0],attrs.fileModelb)
                });
            });
        }
    };
}]).directive('fileModelc', ['$parse', function ($parse,$http) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
        	var model = $parse(attrs.fileModelc);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
            	scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                    scope.fileUploadc( element[0].files[0],attrs.fileModelc)
                });
            });
        }
    };
}]).directive('fileModeld', ['$parse', function ($parse,$http) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
        	var model = $parse(attrs.fileModeld);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
            	scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                    scope.fileUploadd( element[0].files[0],attrs.fileModeld)
                });
            });
        }
    };
}]);

/**
 * Manually bootstrap the Angular module here
 */
angular.element(document).ready(function() {
    angular.bootstrap(document, ["apptha.question"]);
});
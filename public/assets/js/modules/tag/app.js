"use strict";
var tag = angular.module("apptha.tag", []);
tag.directive("notification", notificationDirective);
tag.factory("requestFactory", requestFactory);
tag.directive("baseValidator", validatorDirective);
tag.factory("requestFactory", requestFactory);

tag.controller("TagController", ["$window", "$scope", "requestFactory", "$rootScope", function($window, scope, requestFactory, $rootScope) {
    requestFactory.setThisArgument(this);
    this.tag = {};
    scope.errors = {};

    /**
     * Method to get the rules for the add form
     */
    this.fetchRules = function() {
        requestFactory.get(requestFactory.getUrl("tag/create"), function(response) {
        	baseValidator.setRules(response.rules);
        }, function() {});
    };
    /**
     * Method to save the add form data
     */
    this.save = function(event) {
        if (baseValidator.validateAngularForm(event.target, scope)) {
            this.tag.id = this.id
            this.tag.user_id = 1;
            this.tag.is_active = 1;
            requestFactory.post(requestFactory.getUrl("tag"), this.tag, function(response) {
                $window.location = requestFactory.getTemplateUrl("tag");
                $rootScope.notification.add({isSuccess : true,message : response.message}).showLater();
            }, this.fillError);
        }
    };
    /**
     * Method to handle the error message.
     */
    this.fillError = function(response) {
        this.isDisabled = false;
        if (response.status == 422 && response.data.hasOwnProperty("messages")) {
            angular.forEach(response.data.messages, function(message, key) {
                if (typeof message == "object" && message.length > 0) {
                    scope.errors[key] = {
                        has: true,
                        message: message[0]
                    };
                }
            });
        }
    };
    /**
     * Method to get the single record data for the zones
     *
     * @param id, record id
     * @returns object
     */
    this.fetchSingleInfo = function(id) {
        this.id = id;
        requestFactory.get(requestFactory.getUrl("tag/" + this.id + "/edit"), function(response) {
        	this.tag = response.tagSingleInfo;
        	baseValidator.setRules(response.rules);
        }, function() {});
    };
}]);
/**
 * Manually bootstrap the Angular module here
 */
angular.element(document).ready(function() {
    angular.bootstrap(document, ["apptha.tag"]);
});
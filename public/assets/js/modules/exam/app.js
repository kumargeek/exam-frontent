"use strict";
var dashboard = angular.module("apptha.exam", []);
dashboard.directive("notification", notificationDirective);
dashboard.factory("requestFactory", requestFactory);

dashboard.controller("ExamController", ["$window", "$scope", "requestFactory","$filter","$rootScope","$timeout", function($window, scope, requestFactory, $filter, $rootScope, $timeout) {
	
	var self = this;
	this.dashboard = {};
	this.examimgpath = {};
	this.scrollDownOpt = true;
	this.scrollUpOpt = false;
	this.optionscount = 0;
	
	/**
     * To get the data for the dashboard
     */
    this.init = function() {
    	requestFactory.get(requestFactory.getUrl("getquestions"),function(response) {
    		self.dashboard.questions = response.data.data;
    		
    		self.dashboard.examimgpath = response.data.examimgpath;
    		self.dashboard.optionscollection = response.data.optionscollection;
    		this.dataTemp = JSON.stringify(self.dashboard.optionscollection);
    		
    		
    		
    		
    		localStorage.setItem('questionsset',JSON.stringify(self.dashboard.optionscollection));
    		
    		//self.optionscount = response.data.optionscount;
    		
    	});
    	
    	//requestFactory.get(requestFactory.getUrl("getordercounts"),function(response) {
			self.dashboard.counts = 0;
    	//});
    }
    
   /* this.findObjectByKey = function(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {
                return array[i];
            }
        }
        return null;
    }*/
    
    this.getSingleQuestionView = function(recordId) {
    	
    	//alert(localStorage);
    	
    	this.tempData = localStorage.getItem('questionsset');
    	    	
    	var obj = $filter('filter')(this.tempData, {id: 3}, true)[0];

    	console.log(obj);
    	debugger;
    	
    	//debugger;
    	
    	
    	
    	/** requestFactory.get(requestFactory.getUrl("getsinglequestion/"+recordId), function(response) {
            console.log('welcome');
            exit;
    		// $window.location = requestFactory.getTemplateUrl("question");
           // $rootScope.notification.add({isSuccess : true,message : response.message}).showLater();
        }, this.fillError); */
    }
    
    this.scrollDown = function() {
    	 var container = angular.element(document.getElementById('question-options'));
         container.scrollTop(3000, 5000);
         this.scrollDownOpt = false;
         this.scrollUpOpt = true;
    }
    
    this.range = function() {
    	/**requestFactory.get(requestFactory.getUrl("getquestions"),function(response) {
    		self.optionscount = response.data.optionscount;
    		console.log(self.optionscount);
    	});*/
    	
    	/**step = 1;
        min = 1;
        var input = [];
        for (var i = min; i <= self.optionscount; i += step) {
            input.push(i);
        }
        return input;*/
    }
    
    this.scrollUp = function() {
    	 var container = angular.element(document.getElementById('question-options'));
         container.scrollTop(-3000, 5000);
         this.scrollDownOpt = true;
         this.scrollUpOpt = false;
    }
    
    this.getImage = function(value) {
    	return (value != null) ? value : "assets/images/default-user.png";
    }
    this.fill = function (value) {
    	if(angular.isDefined(value)) {
    		return "width:"+value[0].sum/value[0].count;
    	}else{
    		return "width:0";
    	}
    }
    
    this.examLocalStorage = function(value,id) {
    	if (typeof(Storage) !== "undefined") {
    		localStorage.setItem(id,value);
    		console.log(localStorage);
    	}
    }
    
    this.examSubmit = function() {
    		
    	this.data = { 'result' : localStorage };
    	
    	requestFactory.post(requestFactory.getUrl("examsubmit"),  this.data, function(response) {
            $window.location = requestFactory.getTemplateUrl("dashboard");
            $rootScope.notification.add({isSuccess : true,message : response.message}).showLater();
        });	
    }    
    this.init();
}]);
/**
 * Manually bootstrap the Angular module here
 */
angular.element(document).ready(function() {
    angular.bootstrap(document, ["apptha.exam"]);
});
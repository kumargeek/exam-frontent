"use strict";
var category = angular.module("apptha.category", []);
category.directive("notification", notificationDirective);
category.factory("requestFactory", requestFactory);
category.directive("baseValidator", validatorDirective);
category.factory("requestFactory", requestFactory);

category.controller("CategoryController", ["$window", "$scope", "requestFactory", "$rootScope", function($window, scope, requestFactory, $rootScope) {
    requestFactory.setThisArgument(this);
    this.category = {};
    scope.errors = {};

    /**
     * Method to get the rules for the add form
     */
    this.fetchRules = function() {
        requestFactory.get(requestFactory.getUrl("category/create"), function(response) {
        	baseValidator.setRules(response.rules);
        }, function() {});
    };
    /**
     * Method to save the add form data
     */
    this.save = function(event) {
        if (baseValidator.validateAngularForm(event.target, scope)) {
            this.category.id = this.id
            this.category.user_id = 1;
            this.category.is_active = 1;
            requestFactory.post(requestFactory.getUrl("category"), this.category, function(response) {
                $window.location = requestFactory.getTemplateUrl("category");
                $rootScope.notification.add({isSuccess : true,message : response.message}).showLater();
            }, this.fillError);
        }
    };
    /**
     * Method to handle the error message.
     */
    this.fillError = function(response) {
        this.isDisabled = false;
        if (response.status == 422 && response.data.hasOwnProperty("messages")) {
            angular.forEach(response.data.messages, function(message, key) {
                if (typeof message == "object" && message.length > 0) {
                    scope.errors[key] = {
                        has: true,
                        message: message[0]
                    };
                }
            });
        }
    };
    /**
     * Method to get the single record data for the zones
     *
     * @param id, record id
     * @returns object
     */
    this.fetchSingleInfo = function(id) {
        this.id = id;
        requestFactory.get(requestFactory.getUrl("category/" + this.id + "/edit"), function(response) {
        	this.category = response.categorySingleInfo;
        	baseValidator.setRules(response.rules);
        }, function() {});
    };
}]);
/**
 * Manually bootstrap the Angular module here
 */
angular.element(document).ready(function() {
    angular.bootstrap(document, ["apptha.category"]);
});
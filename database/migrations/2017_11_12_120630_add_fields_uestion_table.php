<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsUestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table ( 'questions', function (Blueprint $table) {
    		$table->text ( 'answer_explanation' )->nullable();
    		$table->text ( 'notes' )->nullable();
    		$table->integer( 'answer_option' )->default(1);
    	} );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table ( 'questions', function (Blueprint $table) {
    		$table->dropColumn ( 'answer_explanation' );
    		$table->dropColumn ( 'notes' );   
    		$table->dropColumn ( 'answer_option' );
    	} );
    }
}

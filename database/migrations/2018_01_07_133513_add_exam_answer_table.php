<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExamAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create ( 'exam_answer', function (Blueprint $table) {
    		$table->engine = 'InnoDB';
    		$table->bigIncrements ( 'id' );
    		$table->unsignedInteger ( 'user_id' );
    		$table->text ( 'answers' );
    		$table->timestamps ();
    	} );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create ( 'questions', function (Blueprint $table) {
    		$table->engine = 'InnoDB';
    		$table->increments ( 'id' );
    		$table->bigInteger( 'category_id' )->default(0);
    		$table->bigInteger( 'tag_id' )->default(0);
    	    $table->bigInteger( 'question_type' )->default(0);
    	    $table->text( 'question_name' )->nullable();
    	    $table->text( 'question_img' )->nullable();
    	    $table->bigInteger( 'question_alignment' )->default(0);
    	    $table->text( 'options' )->nullable();
    		$table->tinyInteger ( 'is_active' )->default ( 1 );
    		$table->bigInteger ( 'creator_id' );
    		$table->bigInteger ( 'updator_id' );
    		$table->timestamps ();
    	} );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drop ( 'tag' );
    }
}

<?php
use Illuminate\Database\Seeder;
use Apptha\Models\SettingCategory;
use Apptha\Models\Setting;
use Apptha\Models\City;
use Apptha\Repositories\SettingRepository;
class SettingsCategoryTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::table ( 'settings' )->delete ();
    DB::table ( 'setting_categories' )->delete ();
    /**
     * Auto increment value set to 1
     */
    DB::unprepared ( "ALTER TABLE setting_categories AUTO_INCREMENT = 1;" );
    DB::unprepared ( "ALTER TABLE settings AUTO_INCREMENT = 1;" );
    
    $settingsCategories = [ 
        '1' => [ 
            'id' => 1,
            'name' => 'General Settings',
            'slug' => 'general_settings',
            'parent_id' => 0,
            'settings' => [ 
                [ 
                    'setting_name' => 'application_name',
                    'setting_value' => 'BB Training',
                    'display_name' => 'Application Name',
                    'type' => 'text',
                    'option' => NULL,
                    'class' => NULL,
                    'order' => 1,
                    'setting_category_id' => 1 
                ],
                [ 
                    'setting_name' => 'logo',
                    'setting_value' => 'logo.png',
                    'display_name' => 'Logo',
                    'type' => 'image',
                    'option' => NULL,
                    'class' => NULL,
                    'order' => 2,
                    'description'=>'Recommended logo resolution is 216px x 38px.',
                    'setting_category_id' => 1 
                ],
                [ 
                    'setting_name' => 'fav',
                    'setting_value' => 'fav.png',
                    'display_name' => 'Fav Icon',
                    'type' => 'image',
                    'option' => NULL,
                    'class' => NULL,
                    'order' => 2,
                    'description'=>'Recommended favicon resolution is 40px x 40px.',
                    'setting_category_id' => 1 
                ],                
                [ 
                    'setting_name' => 'default_timezone',
                    'setting_value' => '',
                    'display_name' => 'Default Timezone',
                    'type' => 'dropdown',
                    'option' => '(UTC+05:30) India,(GMT-11:00) Midway Island, Samoa,(GMT-10:00) Hawaii',
                    'class' => NULL,
                    'order' => 2,
                    'setting_category_id' => 1 
                ],
                [ 
                    'setting_name' => 'default_language',
                    'setting_value' => '',
                    'display_name' => 'Default Language',
                    'type' => 'dropdown',
                    'option' => 'English,Hindi',
                    'class' => NULL,
                    'order' => 2,
                    'setting_category_id' => 1 
                ],
                [ 
                    'setting_name' => 'support_number',
                    'setting_value' => '+91 9791005806',
                    'display_name' => 'Support Number',
                    'type' => 'text',
                    'option' => NULL,
                    'class' => NULL,
                    'order' => 2,
                    'setting_category_id' => 1 
                ],
                [ 
                    'setting_name' => 'support_email_id',
                    'setting_value' => 'example@gmail.com',
                    'display_name' => 'Support Email ID',
                    'type' => 'text',
                    'option' => NULL,
                    'class' => NULL,
                    'order' => 2,
                    'setting_category_id' => 1 
                ],
                [
                  'setting_name' => 'disable_delete',
                  'setting_value' => 'No',
                  'display_name' => 'Disable Delete',
                  'type' => 'dropdown',
                  'option' => 'No,Yes',
                  'class' => NULL,
                  'order' => 2,
                  'setting_category_id' => 1
                ]

            ] 
        ],
    ]
    ;
    foreach ( $settingsCategories as $key => $value ) {
      $setting_category = $value;
      unset ( $setting_category ['settings'] );
      (new SettingCategory ())->fill ( $setting_category )->save ();
      if (isset ( $value ['settings'] ) && count ( $value ['settings'] ) > 0) {
        foreach ( $value ['settings'] as $setting ) {
          (new Setting ())->fill ( $setting )->save ();
        }
      }
    }
    (new SettingRepository ( new Setting (), new SettingCategory () ))->generateSettingsCache ();
        (new SettingRepository ( new Setting (), new SettingCategory () ))->generateValidationCache ();
    }
}

<?php
use Illuminate\Database\Seeder;
use Apptha\Models\EmailTemplate;
class EmailTemplatesTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::table ( 'email_templates' )->delete ();
    DB::unprepared ( "ALTER TABLE email_templates AUTO_INCREMENT = 1;" );
    
    $emailTemplate = [ 
              	'1' => [
        				'name' => 'Registration',
        				'slug' => 'customer-register',
        				'subject' => 'BB Training - Registration Activation',
        				'body' => 'You have succesfully registered, {{EMAIL}} {{PASSWORD}} click on the activation link below.{URL}',
        				'is_active' => 1,
        				'creator_id' => 1,
        				'updator_id' => 1
        		],
        		'2' => [
        				'name' => 'Password Reset',
        				'slug' => 'password-reset',
        				'subject' => 'BB Training - Password Reset',
        				'body' => 'Click on the link below to reset you password, {URL}',
        				'is_active' => 1,
        				'creator_id' => 1,
        				'updator_id' => 1
        		]
        
        		
    ];
    foreach ( $emailTemplate as $key => $value ) {
      EmailTemplate::create ( [
      'id' => $key,
      'name' => $value ['name'],
      'slug' => $value ['slug'],
      'subject' => $value ['subject'],
       'body' => $value ['body'],
      'is_active' => $value ['is_active'],
      'creator_id' => $value ['creator_id'],
      'updator_id' => $value ['updator_id'],
      ] );
    }
  }
}

@extends('layouts.default')

@section('content')
    <div class="contentpanel dashboard" data-ng-controller="ExamController as dashboardCtrl" data-ng-cloak>
        <div class="container">

    <hgroup class="mb20">
		<h1>Examination</h1>
	</hgroup>

    <div class="row">
    <section class="col-xs-12 col-sm-6 col-md-11">
	  <div class="exam-info">
	  ljslkjfljflk ljslkjfljflk ljslkjfljflkljslkjfljflk ljslkjfljflkljslkjfljflk ljslkjfljflk
	  ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflkljslkjfljflk ljslkjfljflk ljslkjfljflk
	  ljslkjfljflk ljslkjfljflkljslkjfljflkv v ljslkjfljflkljslkjfljflk ljslkjfljflk
	  ljslkjfljflk	ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk
	  ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk
	  ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk
	  ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk
	  ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk
	  ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk ljslkjfljflk
	  </div>
	  
	  <div class="exam-start">
        <a href="{{url('exam/info')}}" class="btn btn-success">Start</a>
      </div>
	  
	</section>
	
	</div>
</div>
</div>
@endsection

@section('scripts')    
    <script type="text/javascript" src="{{asset('assets/js/lib/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/lib/jquery.dd.js')}}"></script>    
    <script src="{{asset('assets/js/modules/base/requestFactory.js')}}"></script>
    <script src="{{asset('assets/js/modules/base/notificationDirective.js')}}"></script>
    <script src="{{asset('assets/js/modules/exam/app.js')}}"></script>
     <link rel="stylesheet" href="{{asset('assets/css/exam.css')}}">
@endsection

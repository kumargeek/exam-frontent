@extends('layouts.default')

@section('content')
    <div class="contentpanel dashboard" data-ng-controller="ExamController as dashboardCtrl" data-ng-cloak>
        <div class="container">
   
    <div class="row">
    <section class="col-xs-12 col-sm-6 col-md-8 option-section">
		<div class="student-opt-lists" data-ng-repeat="record in dashboardCtrl.dashboard.questions track by $index" >
		<article class="search-result">
		 <div class="row student-opt-title">
			<div class="col-xs-12 col-sm-12 col-md-7 excerpet" id="expect@{{$index+1}}">
				<h4 class="questions-number"><b>Question No. @{{ $index+1  }}</b></h4>
				<div class="question-img">
				  <img data-ng-if="record.question_img.length > 0" src="@{{record.question_img}}">
				</div>
			</div>
		 </div>
		
		<div class="row exam-question-name">
		   @{{record.question_name}}
		   <div class="scroll-down" data-ng-if="dashboardCtrl.scrollDownOpt" data-ng-click="dashboardCtrl.scrollDown()">
		      <span class="glyphicon glyphicon-arrow-down"></span>
		   </div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 question-options" id="question-options">
				 <div class="row-fluid options-list-div">
					      <div>
					      <span class="option-result">a.</span>
					      <span class="radio">
                             <input type="radio" name="optradio@{{record.id}}" value="1" id="@{{record.id}}" data-ng-click="dashboardCtrl.examLocalStorage(1,record.id)">@{{record.options.option_a}}
                          </span>
                          </div>
					      <span>
					      <img data-ng-if="record.options.option_a_img.length > 0" src="@{{ dashboardCtrl.dashboard.examimgpath }}@{{record.options.option_a_img}}"
					      class="examimg-path">
					      </span>
					    </div>
					    <div class="row options-list-div">
					      <div>
					      <span class="option-result">b.</span>
					      <span class="radio">
                             <input type="radio" name="optradio@{{record.id}}" value="2" id="@{{record.id}}" data-ng-click="dashboardCtrl.examLocalStorage(2,record.id)">@{{record.options.option_b}}
                          </span>
                          </div>
					      <span>
					      <img data-ng-if="record.options.option_b_img.length > 0" src="@{{ dashboardCtrl.dashboard.examimgpath }}@{{record.options.option_b_img}}"
					      class="examimg-path">
					      </span>
					    
				    </div>
				    
				    <div class="row options-list-div">
					      <div>
					      <span class="option-result">c.</span>
					      <span class="radio">
                          <input type="radio" name="optradio@{{record.id}}" value="3" id="@{{record.id}}" data-ng-click="dashboardCtrl.examLocalStorage(3,record.id)">@{{record.options.option_c}}
                          </span>
                          </div>
					      <span>
					      <img data-ng-if="record.options.option_c_img.length > 0" src="@{{ dashboardCtrl.dashboard.examimgpath }}@{{record.options.option_c_img}}"
					      class="examimg-path">
					      </span>
					    </div>
					    <div class="row options-list-div">
					      <div>
					      <span class="option-result">d.</span>
					      <span class="radio">
                             <input type="radio" name="optradio@{{record.id}}" value="4" id="@{{record.id}}" data-ng-click="dashboardCtrl.examLocalStorage(4,record.id)">@{{record.options.option_d}}
                          </span>
                          </div>
					      <span>
					      <img data-ng-if="record.options.option_d_img.length > 0" src="@{{ dashboardCtrl.dashboard.examimgpath }}@{{record.options.option_d_img}}"
					      class="examimg-path">
					      </span>
					    
				    </div>
				    
				    <div class="row" data-ng-if="record.notes.length > 0">
				       <div class="exam-notes">
				          <p>Note : @{{record.notes}} </p>
				       </div>
				    </div>
				    
			</div>
			
		</div>
		<div class="scroll-up" data-ng-if="dashboardCtrl.scrollUpOpt" data-ng-click="dashboardCtrl.scrollUp()">
		      <span class="glyphicon glyphicon-arrow-up"></span>
		</div>
		</article>
       </div>
       <div class="row-fluid btm-review-clear">
		  <div class="col-md-10">
		    <div class="col-md-4">
		      <div class="opts-review-nxt" data-ng-click="dashboardCtrl.markReviewNext()">Mark for Review & Next</div>
		    </div>
		    <div class="col-md-4">
		      <div class="opts-clear-response" data-ng-click="dashboardCtrl.clearResponse()">Clear Response</div>
		    </div>
		  </div>
		  <div class="col-md-2">
		      <div class="opts-save-nxt" data-ng-click="dashboardCtrl.examSubmit()">Save & Next</div>
		  </div>
		</div>
	</section>
	<section class="col-md-4">
	  <div class="timer-section">
	     {{Carbon\Carbon::now()->timezone('Asia/Kolkata')->format('h:i A')}}
	  </div>
	  <div>
	  <div class="exam-head-info">
	    <div class="exam-answer exam-info-answered"><span class="exam-answered">0</span> Answered</div>
	    <div class="exam-answer exam-info-notanswered"><span class="exam-not-answered">0</span> Not Answered</div>
	    <div class="exam-answer exam-info-notvisited"><span class="exam-not-visited">0</span> Not Visited</div>
	    <div class="exam-answer exam-info-markedreview"><span class="exam-marked-review">0</span> Marked for reviewed</div>
	    <div class="exam-answer exam-info-answerreview"><span class="exam-answer-review">0</span> 
	    <span class="exam-answer-review-detail">Answered & Marked for Review (will be consider for evaluation)</span></div>
	  </div>
	  <div class="exam-head-title">
	       <h4>Examination</h4>
	    </div>
	  <div class="option-list">	    
	    <h5>Choose a Question</h5>
	    <div class="option-answer-btn">
	    <div class="option-one" id="expect1" data-ng-repeat="record in dashboardCtrl.dashboard.optionscollection track by $index">
	      <div data-ng-click="dashboardCtrl.getSingleQuestionView(record.id)">
	         @{{$index+1}}
	      </div>
	    </div>
	    </div>	    
	  </div>
	  </div>
	</section>
	</div>
</div>
</div>
@endsection

@section('scripts')    
    <script type="text/javascript" src="{{asset('assets/js/lib/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/lib/jquery.dd.js')}}"></script>    
    <script src="{{asset('assets/js/modules/base/requestFactory.js')}}"></script>
    <script src="{{asset('assets/js/modules/base/notificationDirective.js')}}"></script>
    <script src="{{asset('assets/js/modules/exam/app.js')}}"></script>
    
    <script src="{{asset('assets/js/modules/exam/timercount.js')}}"></script>
    
     <link rel="stylesheet" href="{{asset('assets/css/exam.css')}}">
@endsection

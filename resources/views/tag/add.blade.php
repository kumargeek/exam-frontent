@extends('layouts.default')
@section('content')
@section('section-heading')
 {{ trans('email.edit_email_template') }}
@endsection
<div class="contentpanel">
   <div class="content-header clearfix">
      <h2>Add Tag</h2>
   </div>
    @include('layouts.errors')
    <div class="content clearfix" data-ng-controller="TagController as tagCtrl" data-ng-init="tagCtrl.fetchRules()">
            <form method="post" data-base-validator data-ng-submit = "tagCtrl.save($event)" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-page-bg">
            <div class="form-page clearfix">
                <div class="col-md-12">
                <div class="form-group" data-ng-class="{'has-error': errors.name.has}">
                        <div class="col-md-2">
                            <label class="label-name">Tag Name<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control"  name="name" data-ng-model="tagCtrl.tag.name" />
                            <p class="help-block" data-ng-show="errors.name.has">@{{ errors.name.message }}</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="form-page clearfix">
                      <div class="col-md-8">
                          <div class="col-md-4">
                          </div>
                          <div class="col-md-8">
                              <input type="submit" class="btn blue-button" value="Submit"/>
                              <a href="{{url('tag')}}" class="btn-default btn">{{ trans('general.cancel') }}</a>
                          </div>
                      </div>
                  </div>
            </form>
        </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('assets/js/modules/base/requestFactory.js')}}"></script> 
<script src="{{asset('assets/js/modules/base/Validate.js')}}"></script>
<script src="{{asset('assets/js/modules/base/validatorDirective.js')}}"></script>
<script src="{{asset('assets/js/modules/base/notificationDirective.js')}}"></script>
<script src="{{asset('assets/js/modules/tag/app.js')}}"></script>
@endsection
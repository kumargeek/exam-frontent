@extends('layouts.default')

@section('content')
    <div class="contentpanel dashboard" data-ng-controller="DashboardController as dashboardCtrl" data-ng-cloak>
        <div class="row" data-ng-hide="true">
            <div class="col-md-6">
                <div class="total-order">
                    <div class="green col-md-4 nopadding">
                        <span>@{{dashboardCtrl.dashboard.targetOrder.target}}</span>
                        <h4>{{trans('dashboard.target_order')}}</h4>
                    </div>
                    <div class="yellow col-md-8 nopadding">
                        <div class="target">
                            <span>@{{dashboardCtrl.dashboard.counts.totalOrder}}</span>
                            <h4>{{trans('dashboard.total_order')}}</h4>
                        </div>
                        <div class="archived">
                            <span>@{{dashboardCtrl.dashboard.counts.orderProcessed}}</span>
                            <h4>{{trans('dashboard.achieved')}}</h4>
                        </div>
                        <p>{{Carbon\Carbon::now()->format('F')}} month</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="revenue">
                    <div class="desc">
                        
                        <h4>{{trans('dashboard.total_revenue')}}</h4>
                    </div>
                    <i class="icon-total-revenue"></i>
                </div>
            </div>
            <div class="col-md-3">
                <div class="total-users">
                    <h2>{{trans('dashboard.total_users')}}</h2>
                    <div class="division">
                        <div class="drivers" data-ng-hide="true">
                            <span>@{{dashboardCtrl.dashboard.driver.driverCount}}  <i class="icon-car-steering-wheel"></i></span>
                            <h4>{{trans('dashboard.drivers')}}</h4>
                        </div>
                        <div class="customers">
                            <span>@{{dashboardCtrl.dashboard.customer.customerCount}} <i class="icon-manage-user"></i></span>
                            <h4>{{trans('dashboard.customers')}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="order-by-time">
            <h2>{{trans('dashboard.orders_by_time')}} <span>{{Carbon\Carbon::now()->timezone('Asia/Kolkata')->format('h:i A')}}</span></h2>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>{{trans('dashboard.duration')}}</th>
                            <th class="text-center">{{trans('dashboard.pickup')}}</th>
                            <th class="text-center">{{trans('dashboard.delivery')}}</th>
                            <th class="text-center">{{trans('dashboard.missed')}}</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{trans('dashboard.1_hour')}}</td>
                            <td class="text-center" data-ng-if="dashboardCtrl.dashboard.pickupCounts.pickupOneHour"><a>@{{dashboardCtrl.dashboard.pickupCounts.pickupOneHour}}</a></td>
                            <td class="text-center" data-ng-if="!dashboardCtrl.dashboard.pickupCounts.pickupOneHour"><a>0</a></td>
                            <td class="text-center" data-ng-if="dashboardCtrl.dashboard.deliveryCounts.deliveryOneHour"><a>@{{dashboardCtrl.dashboard.deliveryCounts.deliveryOneHour}}</a></td>
                            <td class="text-center" data-ng-if="!dashboardCtrl.dashboard.deliveryCounts.deliveryOneHour"><a>0</a></td>
                            <td class="text-center" data-ng-if="dashboardCtrl.dashboard.missedCounts.missedOneHour"><a>@{{dashboardCtrl.dashboard.missedCounts.missedOneHour}}</a></td>
                            <td class="text-center" data-ng-if="!dashboardCtrl.dashboard.missedCounts.missedOneHour"><a>0</a></td>
                            </tr>
                        <tr>
                            <td>{{trans('dashboard.2_hour')}}</td>
                            <td class="text-center" data-ng-if="dashboardCtrl.dashboard.pickupCounts.pickupTwoHour"><a>@{{dashboardCtrl.dashboard.pickupCounts.pickupTwoHour}}</a></td>
                            <td class="text-center" data-ng-if="!dashboardCtrl.dashboard.pickupCounts.pickupTwoHour"><a>0</a></td>
                            <td class="text-center" data-ng-if="dashboardCtrl.dashboard.deliveryCounts.deliveryTwoHour"><a>@{{dashboardCtrl.dashboard.deliveryCounts.deliveryTwoHour}}</a></td>
                            <td class="text-center" data-ng-if="!dashboardCtrl.dashboard.deliveryCounts.deliveryTwoHour"><a>0</a></td>
                            <td class="text-center" data-ng-if="dashboardCtrl.dashboard.missedCounts.missedTwoHour"><a>@{{dashboardCtrl.dashboard.missedCounts.missedTwoHour}}</a></td>
                            <td class="text-center" data-ng-if="!dashboardCtrl.dashboard.missedCounts.missedTwoHour"><a>0</a></td>
                            </tr>
                        <tr>
                            <td>{{trans('dashboard.3_hour')}}</td>
                            <td class="text-center" data-ng-if="dashboardCtrl.dashboard.pickupCounts.pickupThreeHour"><a>@{{dashboardCtrl.dashboard.pickupCounts.pickupThreeHour}}</a></td>
                            <td class="text-center" data-ng-if="!dashboardCtrl.dashboard.pickupCounts.pickupThreeHour"><a>0</a></td>
                            <td class="text-center" data-ng-if="dashboardCtrl.dashboard.deliveryCounts.deliveryThreeHour"><a>@{{dashboardCtrl.dashboard.deliveryCounts.deliveryThreeHour}}</a></td>
                            <td class="text-center" data-ng-if="!dashboardCtrl.dashboard.deliveryCounts.deliveryThreeHour"><a>0</a></td>
                            <td class="text-center" data-ng-if="dashboardCtrl.dashboard.missedCounts.missedThreeHour"><a>@{{dashboardCtrl.dashboard.missedCounts.missedThreeHour}}</a></td>
                            <td class="text-center" data-ng-if="!dashboardCtrl.dashboard.missedCounts.missedThreeHour"><a>0</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>   
@endsection

@section('scripts')    
    <script type="text/javascript" src="{{asset('assets/js/lib/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/lib/jquery.dd.js')}}"></script>    
    <script src="{{asset('assets/js/modules/base/requestFactory.js')}}"></script>
    <script src="{{asset('assets/js/modules/base/notificationDirective.js')}}"></script>
    <script src="{{asset('assets/js/modules/dashboard/app.js')}}"></script>
@endsection
<div class="content-header clearfix">
    <h2 class="col-md-6 col-sm-6 col-xs-6 nopadding">Question Bank Management</h2>
    <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
        <a href="{{url('question/add')}}" class="button blue-button">Add Quesiton</a>
    </div>
</div>
<div class="table-tab">
    <div class="three_icons">
        <i class="fa fa-bars"></i>
        <i class="fa fa-times" style="display: none"></i>
    </div>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active">
            <div class="table-responsive">
                <div id="table_loader" class="table_loader_container ng-hide"
                 data-ng-show="tableLoader">
                 <div class="table_loader">
                  <div class="loader"></div>
                 </div>
                </div>
                <table class="table tablesaw" data-tablesaw-mode="columntoggle">
                    <thead>
                            <th>{{trans('general.sno')}}</th>
                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Question Name</th>
                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Category Name</th>
                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Tag Name</th>
                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Question Type</th>
                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">{{trans('general.action')}}</th>
                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="search-row">
                            <td></td>
                            <td>
                                <div class="tooltip1">
                                    <input type="text" class="form-control" data-ng-model="filters.name">
                                    <span class="tooltiptext">Question Name</span>
                                </div>
                            </td>
                            <td>
                            
                            <div class="tooltip1">                                                        
                                                    <select class="form-control select" data-ng-change="getRecords(true)" 
                                                    data-ng-model="filters.category_id" 
                                                    data-ng-options="category.id as category.name for category in moreInfo.categories">
                                                    <option value="">All</option>
                                                    </select>
                                                    <span class="tooltiptext">Category</span>
                            </div>
                            
                            
                            
                            </td>
                            <td>
                            <select class="form-control select"
                            data-ng-change="getRecords(true)"
                            name="tag_id" data-validation-name="Category" 
                            data-ng-model="filters.tag_id"
                            data-ng-options="tag.id as tag.name for tag in moreInfo.tags">
                            <option value="">All</option>
                            </select>
                            </td>
                            <td></td>
                            
                            
                             <td><button type="button" data-ng-click="doGridSearch()"
        class="btn search warning" data-boot-tooltip="true"
        data-toggle="tooltip"
        data-original-title="{{trans('general.search')}}">
        <i class="fa fa-search"></i></button>
        <button type="button" class="danger product_list_reset_btn" name="reset" data-ng-click="gridReset()" value="{{trans('general.reset')}}" data-boot-tooltip="true" data-toggle="tooltip" data-original-title="{{trans('general.reset')}}"><i class="fa fa-refresh"></i></button>
        </td>
                            <td></td>
                        </tr>
                        <tr data-ng-if="noRecords">
                            <td colspan="7" class="no-data">{{trans('messages.empty_record')}}</td>
                        </tr>
                                                
@{{records}}
                        
                        <tr data-ng-if="showRecords" data-on-finish-rendered data-ng-repeat="record in records track by $index">
                            <td>@{{$index+1}}</td>
                            <td>@{{record.question_name}}</td>
                            
                            <td >
                            
                            <span data-ng-repeat="categoryname in moreInfo.categories" data-ng-if="categoryname.id==record.category_id">@{{categoryname.name}}</span>
                             
                            
                            <td> <span data-ng-repeat="tagname in moreInfo.tags" data-ng-if="tagname.id==record.tag_id">@{{tagname.name}}</span>
                            
                            </td>
                            <td>Fill in the Blank</td>
                            <td>
                              <a ng-href="{{url('question/edit').'/'}}@{{record.id}}">
                               <i class="icon-edit grey"></i>
                              </a>
                              
                              <a href="#" data-toggle="modal" data-target="#deleteModal" data-ng-click="deleteSingleRecord(record.id)">
                               <i class="icon-delete grey"></i>
                              </a>
                              
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @include('layouts.pagination',['module_name'=>'Question'])
        </div>
    </div>
</div>



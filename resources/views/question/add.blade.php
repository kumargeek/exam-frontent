@extends('layouts.default')
@section('content')
@section('section-heading')
 {{ trans('email.edit_email_template') }}
@endsection
<div class="contentpanel">
   <div class="content-header clearfix">
      <h2>Add Question</h2>
   </div>
    @include('layouts.errors')
    <div class="content clearfix" data-ng-controller="QuestionController as questionCtrl" data-ng-init="questionCtrl.fetchRules()">
            <form method="post" name="adminForm" enctype="multipart/form-data" data-base-validator data-ng-submit = "questionCtrl.save($event)">
            {{ csrf_field() }}
            <div class="form-page-bg">
            <div class="form-page clearfix">
                <div class="col-md-12">
                <div class="form-group" data-ng-class="{'has-error': errors.category_id.has}">
                        <div class="col-md-3">
                            <label class="label-name">Category Name<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                         <select class="form-control select" name="category_id"
                         data-ng-init="questionCtrl.question.category_id = 1" 
                         data-validation-name="Category" 
                         data-ng-model="questionCtrl.question.category_id"
                         data-ng-options="category.id as category.name for category in questionCtrl.categories">
                         </select>
                         <p class="help-block" data-ng-show="errors.category_id.has">@{{ errors.category_id.message }}</p>
                        </div>
                    </div>
                    <div class="form-group" data-ng-class="{'has-error': errors.tag_id.has}">
                        <div class="col-md-3">
                            <label class="label-name">Tag Name<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control select" 
                            data-ng-init="questionCtrl.question.tag_id = 1"
                            name="tag_id" data-validation-name="Category" 
                            data-ng-model="questionCtrl.question.tag_id"
                            data-ng-options="tag.id as tag.name for tag in questionCtrl.tags">
                            </select>
                            
                            <p class="help-block" data-ng-show="errors.tag_id.has">@{{ errors.tag_id.message }}</p>
                        </div>
                    </div>
                    <div class="form-group" data-ng-class="{'has-error': errors.question_type.has}">
                        <div class="col-md-3">
                            <label class="label-name">Question Type<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                        
                        <select class="form-control select"                            
                            name="question_type" 
                            data-ng-init="questionCtrl.question.question_type = 1"
                            data-ng-model="questionCtrl.question.question_type"
                            data-ng-options="tag.id as tag.name for tag in questionCtrl.questionType">
                            </select>  
                            
                            <p class="help-block" data-ng-show="errors.question_type.has">@{{ errors.question_type.message }}</p>
                        </div>
                    </div>
                    <div class="form-group" data-ng-class="{'has-error': errors.question_name.has}">
                        <div class="col-md-3">
                           <label class="label-name">Question Name<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" name="question_name" data-ng-model="questionCtrl.question.question_name" />
                            <p class="help-block" data-ng-show="errors.question_name.has">@{{ errors.question_name.message }}</p>                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-3">
                           <label class="label-name">Question Image Upload</label>
                        </div>
                        <div class="col-md-6">
                           
                            
                            <input
											name="question_image" type="file"
											data-file-model="question_image"
											data-ng-model="questionCtrl.question.question_image"
											id="doc_profile_image" class="">
                        </div>
                    </div>
                    
                    <div class="form-group" data-ng-class="{'has-error': errors.question_alignment.has}">
                        <div class="col-md-3">
                           <label class="label-name">Question Image Alignment</label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control select"                            
                            name="question_alignment" 
                            data-ng-init="questionCtrl.question.question_alignment = 1"
                            data-ng-model="questionCtrl.question.question_alignment"
                            data-ng-options="tag.id as tag.name for tag in questionCtrl.questionAlignment">
                            </select>  
                        </div>
                    </div>
                    
                    <div class="form-group well" data-ng-class="{'has-error': errors.option_a.has}">
                        <div class="col-md-3">
                           <label class="label-name">Options A<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <input class="form-control" name="option_a" data-ng-model="questionCtrl.question.option_a" />
                            <p class="help-block" data-ng-show="errors.question_option_a.has">@{{ errors.question_option_a.message }}</p>
                            </div>
                            <div class="form-group">
                            <input type="file" name="option_a_img" 
                            data-file-modela="option_a_img"
                            data-ng-model="questionCtrl.question.option_a_img">
                            </div>
                            <div class="form-group">
                            <select class="form-control select"                            
                            name="option_a_alignment" 
                            data-ng-init="questionCtrl.question.option_a_alignment = 1"
                            data-ng-model="questionCtrl.question.option_a_alignment"
                            data-ng-options="tag.id as tag.name for tag in questionCtrl.questionOptAlignment">
                            </select>       
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="form-group well" data-ng-class="{'has-error': errors.option_b.has}">
                        <div class="col-md-3">
                           <label class="label-name">Options B<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <input class="form-control" name="option_b" data-ng-model="questionCtrl.question.option_b" />
                            <p class="help-block" data-ng-show="errors.question_option_b.has">@{{ errors.question_option_b.message }}</p>
                            </div>
                            <div class="form-group">
                            <input type="file" name="option_b_img" 
                            data-file-modelb="option_b_img"
                            data-ng-model="questionCtrl.question.option_b_img">
                            </div>
                            <div class="form-group">
                            <select class="form-control select"                            
                            name="option_b_alignment" 
                            data-ng-init="questionCtrl.question.option_b_alignment = 1"
                            data-ng-model="questionCtrl.question.option_b_alignment"
                            data-ng-options="tag.id as tag.name for tag in questionCtrl.questionOptAlignment">
                            </select>                              
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="form-group well" data-ng-class="{'has-error': errors.option_c.has}">
                        <div class="col-md-3">
                           <label class="label-name">Options C<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <input class="form-control" name="option_c" data-ng-model="questionCtrl.question.option_c" />
                            <p class="help-block" data-ng-show="errors.question_option_c.has">@{{ errors.question_option_c.message }}</p>
                            </div>
                            <div class="form-group">
                            <input type="file" name="option_c_img" 
                            data-file-modelc="option_c_img"
                            data-ng-model="questionCtrl.question.option_c_img">
                            </div>
                            <div class="form-group">
                            <select class="form-control select"                            
                            name="option_c_alignment" 
                            data-ng-init="questionCtrl.question.option_c_alignment = 1"
                            data-ng-model="questionCtrl.question.option_c_alignment"
                            data-ng-options="tag.id as tag.name for tag in questionCtrl.questionOptAlignment">
                            </select>                          
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="form-group well" data-ng-class="{'has-error': errors.option_d.has}">
                        <div class="col-md-3">
                           <label class="label-name">Options D<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <input class="form-control" name="option_d" data-ng-model="questionCtrl.question.option_d" />
                            <p class="help-block" data-ng-show="errors.question_option_d.has">@{{ errors.question_option_d.message }}</p>
                            </div>
                            <div class="form-group">
                            <input type="file" name="option_d_img" 
                            data-file-modeld="option_d_img"
                            data-ng-model="questionCtrl.question.option_d_img">
                            </div>
                            <div class="form-group">
                            <select class="form-control select"                            
                            name="option_d_alignment" 
                            data-ng-init="questionCtrl.question.option_d_alignment = 1"
                            data-ng-model="questionCtrl.question.option_d_alignment"
                            data-ng-options="tag.id as tag.name for tag in questionCtrl.questionOptAlignment">
                            </select>                            
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="form-group" data-ng-class="{'has-error': errors.answer_option.has}">
                        <div class="col-md-3">
                            <label class="label-name">Answer Option<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <select class="form-control select"
                            name="answer_option" 
                            data-ng-init="questionCtrl.question.answer_option = 1"
                            data-ng-model="questionCtrl.question.answer_option"
                            data-ng-options="tag.id as tag.name for tag in questionCtrl.questionAnswers">
                        </select> 
                            <p class="help-block" data-ng-show="errors.answer_option.has">@{{ errors.answer_option.message }}</p>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="label-name">Answer Explanation</label>
                        </div>
                        <div class="col-md-6">
                            <textarea class="form-control" name="answer_explanation" data-ng-model="questionCtrl.question.answer_explanation" > 
                            </textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="label-name">Notes<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                            <textarea class="form-control" name="notes" data-ng-model="questionCtrl.question.notes" >
                            </textarea>
                        </div>
                    </div>
                    
                    <div class="form-group" data-ng-class="{'has-error': errors.answer_option.has}">
                        <div class="col-md-3">
                            <label class="label-name">Status<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-md-6">
                        
                        <select class="form-control select"
                            name="is_active" 
                            data-ng-init="questionCtrl.question.is_active = 1"
                            data-ng-model="questionCtrl.question.is_active"
                            data-ng-options="tag.key as tag.value for tag in questionCtrl.statusDropDown">
                        </select> 
                        
                            
                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="form-page clearfix">
                      <div class="col-md-8">
                          <div class="col-md-4">
                          </div>
                          <div class="col-md-8">
                              <input type="submit" class="btn blue-button" value="Submit"/>
                              <a href="{{url('question')}}" class="btn-default btn">{{ trans('general.cancel') }}</a>
                          </div>
                      </div>
                  </div>
            </form>
        </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('assets/js/modules/base/requestFactory.js')}}"></script> 
<script src="{{asset('assets/js/modules/base/Validate.js')}}"></script>
<script src="{{asset('assets/js/modules/base/validatorDirective.js')}}"></script>
<script src="{{asset('assets/js/modules/base/notificationDirective.js')}}"></script>
<script src="{{asset('assets/js/modules/question/app.js')}}"></script>
@endsection
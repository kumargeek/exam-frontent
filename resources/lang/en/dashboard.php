<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Manage customer Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'target_order' => 'Total Exams',
    'total_order' => 'Total Students',
    'achieved' => 'Achieved',
    'total_revenue' => 'Total Exam Banks',
    'total_users' => 'Total ',
    'drivers' => 'Drivers',
    'customers' => 'Total Overall Students',
    'orders_by_time' => 'Exam Time:',
    'duration' => 'Exam Name',
    'pickup' => 'Total Students',
    'delivery' => 'Duration',
    'missed' => 'Status',
    'pending' => 'Pending',
    '1_hour' => 'Group I',
    '2_hour' => 'Group II',
    '3_hour' => 'Group III',
    'status_overview' => 'Status Overview',
    'instant' => 'Instant',
    'scheduled' => 'Scheduled',
    'same_day' => 'Same Day',
    'next_day' => 'Next Day',
    'create_order' => 'Create Order',
    'activity_analytics' => 'Activity analytics',
    'delivery_performance' => 'Delivery performance',
    'customer' => 'Students',
    'parcel' => 'Exams',
    'driver_review_performance' => 'Student Review Performance',
    'outstanding' => 'Outstanding',
    'excellent' => 'Excellent',
    'good' => 'Good',
    'poor' => 'Poor',
    'to_be_avoided' => 'To be avoided',
    'vistors_overview' => 'Vistors Overview',
    'by_today' => 'By today',
    'new_vistor' => 'New Vistor',
    'returning_vistor' => 'Returning Vistor',
    'sessions' => 'Sessions',
    'desktop' => 'Desktop',
    'mobile' => 'Mobile',
    'order_delivery' => 'Order Delivery',
    'recent' => 'Recent',
    'driver' => 'Driver',
    'location' => 'Location',
    'type' => 'Type',
    'delivery_time' => 'Delivery Time',
    'status' => 'Status',
    'company' => 'Company',
    'view_all' => 'View All',
    'driver_info' => 'Driver Info',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'no_of_services' => 'No Of Services',
    'ratings' => 'Ratings',
];

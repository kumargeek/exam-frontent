<?php

/**
 * Tag model
 *
 * @name       C2Theme
 * @version    1.0
 * @author     C2Theme Team <developers@c2theme.com>
 * @copyright  Copyright (C) 2016 Contus. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Apptha\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {
 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'questions';
 /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
 protected $fillable = [ 
   'category_id',
   'tag_id',
   'question_type',
   'question_name',
   'question_img',
   'question_alignment',
   'options',
   'is_active',
   'creator_id',
   'updator_id'
 ];
}

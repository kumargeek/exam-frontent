<?php

/**
 * question Repository 
 *
 * To manage question related actions.
 *
 * @name       C2Theme
 * @version    1.0
 * @author     C2Theme Team <developers@c2theme.com>
 * @copyright  Copyright (C) 2016 Contus. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */

namespace Apptha\Repositories;
use Illuminate\Database\QueryException;
use Apptha\Models\Question;
use Contus\Base\Repositories\Repository;
use Contus\Base\Exceptions\InvalidRequestException;
use Apptha\Models\Category;
use Apptha\Models\Tag;
use Apptha\Models\ExamAnswer;
use Auth;

class QuestionRepository extends Repository {
/**
 * Class initializer
 *
 * @return void
 */
public function __construct(Question $question, ExamAnswer $examAnswer) {
 parent::__construct ();
 $this->question = $question;
 $this->examAnswer = $examAnswer;
}
/**
 * This method is use to save the data in email templates tables
 *
 * @see \\Contus\Base\Contracts\ResourceInterface::store()
 *
 * @return boolean
 */
public function store() {
 return $this->addOrUpdate ( $this->request->all () );
}

/**
 * This method is use to update the email templates
 *
 * @see \Contus\Base\Contracts\ResourceInterface::update()
 * @return boolean
 */
public function update() {
 return $this->addOrUpdate ( $this->request->all (), $this->request->id );
}



/**
 * This method is use as a common method for both store and update email templates 
 *
 * @param array $requestData         
 * @param int $id         
 * @return boolean
 */
public function addOrUpdate($requestData, $id = null) {
  $operationStatus=true;
   if (! empty ( $id )) {
     $question = $this->question->find ( $id );
     $this->setRule ( 'question_name', 'required|unique:question,question_name,' . $question->id );
    } else {
     $question = $this->question;
     $this->setRule ( 'question_name', 'required|unique:question,question_name' );
    }
    $this->setRule ( 'is_active', 'required|numeric' );
    $this->_validate ();
    $question->fill ( $this->request->all() );
    $question->fill ( array(
                                'creator_id' => 1,
                                'updator_id' => 1
                         ) );
    
    app('log')->info(print_r($this->request->all(),1));
    
    $destinationPath = public_path () . '/assets/temp/';
    
    $optaImg = $optbImg = $optcImg = $optdImg = ''; 
    
    if($this->request->has('option_a_img')) {
      $optaImg = $this->request->option_a_img;
    }
    
    if($this->request->has('option_b_img')) {
        $optbImg = $this->request->option_b_img;
    }
    
    if($this->request->has('option_c_img')) {
        $optcImg = $this->request->option_c_img;
    }
    
    if($this->request->has('option_d_img')) {
        $optdImg = $this->request->option_d_img;
    }
    $optionsArray = ['option_a' => $this->request->option_a,
            'option_a_img' => $optaImg,
            'option_a_alignment' => $this->request->option_a_alignment,
            'option_b' => $this->request->option_b,
            'option_b_img' => $optbImg,
            'option_b_alignment' => $this->request->option_b_alignment,
            'option_c' => $this->request->option_c,
            'option_c_img' => $optcImg,
            'option_c_alignment' => $this->request->option_c_alignment,
            'option_d' => $this->request->option_d,
            'option_d_img' => $optdImg,
            'option_d_alignment' => $this->request->option_d_alignment,
    ];
    
    $optionsJson = json_encode($optionsArray);
    $question->options = $optionsJson;
    $question->question_img = $this->request->question_image;
    $question->answer_explanation = $this->request->answer_explanation;
    $question->notes = $this->request->notes;
    $question->is_active = $this->request->is_active;
    $question->answer_option = $this->request->answer_option;
    $question->save ();
    return $operationStatus;
}

public function uploadFile() {    
    /**
   * this variable to going to hold the user profile picture
   * upload directory
   *
   * @var string $destinationPath
   */
  $destinationPath = public_path () . '/assets/temp/';
  
  if ($this->request->hasFile ( 'question_image' )) {
   $this->validate ( $this->request, [ 
           'question_image' => 'mimes:jpeg,png' 
   ] );
   $fileName = str_random ( 3 ) . time () . $this->request->question_image->getClientOriginalName ();
   $this->request->file ( 'question_image' )->move ( $destinationPath, $fileName );
   return [ 
     'status' => true,
     'filename' => $fileName 
   ];
  } else {
   return [ 
     'status' => false,
     'code' => 422 
   ];
  }
  } 
  
  public function uploadFileA() {
      $destinationPath = public_path () . '/assets/temp/';
  
      if ($this->request->hasFile ( 'option_a_img' )) {
          $this->validate ( $this->request, [
                  'option_a_img' => 'mimes:jpeg,png'
          ] );
          $fileName = str_random ( 3 ) . time () . $this->request->option_a_img->getClientOriginalName ();
          $this->request->file ( 'option_a_img' )->move ( $destinationPath, $fileName );
          return [
                  'status' => true,
                  'filename' => $fileName
          ];
      } else {
          return [
                  'status' => false,
                  'code' => 422
          ];
      }
  } 
  

public function uploadFileB() {

    $destinationPath = public_path () . '/assets/temp/';
      if ($this->request->hasFile ( 'option_b_img' )) {
          $this->validate ( $this->request, [
                  'option_b_img' => 'mimes:jpeg,png'
          ] );
          $fileName = str_random ( 3 ) . time () . $this->request->option_b_img->getClientOriginalName ();
          $this->request->file ( 'option_b_img' )->move ( $destinationPath, $fileName );
          return [
                  'status' => true,
                  'filename' => $fileName
          ];
      } else {
          return [
                  'status' => false,
                  'code' => 422
          ];
      }
  } 


public function uploadFileC() {
    $destinationPath = public_path () . '/assets/temp/';
if($this->request->hasFile ( 'option_c_img' )) {
      if ($this->request->hasFile ( 'option_c_img' )) {
          $this->validate ( $this->request, [
                  'option_c_img' => 'mimes:jpeg,png'
          ] );
          $fileName = str_random ( 3 ) . time () . $this->request->option_c_img->getClientOriginalName ();
          $this->request->file ( 'option_c_img' )->move ( $destinationPath, $fileName );
          return [
                  'status' => true,
                  'filename' => $fileName
          ];
      } else {
          return [
                  'status' => false,
                  'code' => 422
          ];
      }
  } 
}

public function uploadFileD() {
    $destinationPath = public_path () . '/assets/temp/';
if($this->request->hasFile ( 'option_d_img' )) {
      if ($this->request->hasFile ( 'option_d_img' )) {
          $this->validate ( $this->request, [
                  'option_d_img' => 'mimes:jpeg,png'
          ] );
          $fileName = str_random ( 3 ) . time () . $this->request->option_d_img->getClientOriginalName ();
          $this->request->file ( 'option_d_img' )->move ( $destinationPath, $fileName );
          return [
                  'status' => true,
                  'filename' => $fileName
          ];
      } else {
          return [
                  'status' => false,
                  'code' => 422
          ];
      }
  }
}

public function setFileUploadProcess() {
    
}

/**
 * Prepare the grid
 * set the grid model and relation model to be loaded
 *
 * @return \Contus\Base\Repositories\Repository
 */
public function prepareGrid() {
 $this->setGridModel ( $this->question );
 return $this;
}

/**
 * Update grid records collection query
 *
 * @param mixed $pushNotification         
 * @return mixed
 */
protected function updateGridQuery($question) {
 /**
  * updated the grid query by using this function and apply the is_active condition.
  */
 $filters = $this->request->input('filters');
 if (! empty ( $filters )) {
  foreach ( $filters as $key => $value ) {
   switch ($key) {
    case 'name' :
     $question->where ( 'question_name', 'like', '%' . $value . '%' )->get ();
     break;
     case 'category_id' :
         $question->where ( 'category_id', $value)->get ();
         break;
         case 'tag_id' :
             $question->where ( 'tag_id', $value )->get ();
             break;
    default :
     $question->where ( 'is_active', 1 )->orWhere('is_active', 0);
     break;
   }
  }
 }
 return $question;
}

/**
 * This method is use to soft delete the records
 *
 * @see \Contus\Base\Contracts\ResourceInterface::destroy()
 *
 * @return bool
 */
public function destroy() {
return $this->question->where ( 'id', $this->request->id )->update ( array (
  'is_active' => 0 
 ) );
}
/**
 * Method to get the data of single record of email template
 *
 * @see \Contus\Base\Contracts\ResourceInterface::edit()
 * @return array, id, list of email
 */
public function edit($id) {

 return array (
   'id' => $id,
   'questionSingleInfo' => $this->question->where ( 'id', $id )->first (),
   'rules' => array (
     'name' => 'required'
   )
 );
}
/**
 * Method to get the rules of email template
 *
 * @see \Contus\Base\Contracts\ResourceInterface::create()
 * @return array
 */
public function create() {
 return array (
   'rules' => array (
     'category_id' => 'required',
        'tag_id' => 'required',
        'question_type' => 'required',
        'question_name' => 'required',
        'option_a' => 'required',
           'option_b' => 'required',
           'option_c' => 'required',
           'option_d' => 'required',
           'answer_option' => 'required'
   ),
   'categories' => Category::where('is_active',1)->get(),
   'tags' => Tag::where('is_active',1)->get(),
 );
}

/**
 * Get additional information for grid
 * 
 * helper method helps to append more information to grid
 *
 * @return array
 *
 */
public function getGridAdditionalInformation() {
    return ['categories' => Category::where('is_active',1)->get(),
   'tags' => Tag::where('is_active',1)->get()];
}

public function editQuestion() {
    $optionsData = $this->question->where ( 'id', $this->request->id )->first ();
    $options = json_decode($optionsData->options);
    return array (
            'id' => $this->request->id,
            'questionSingleInfo' => $this->question->where ( 'id', $this->request->id )->first (),
            'options' => $options,
            'rules' => array (
                    'category_id' => 'required',
            'tag_id' => 'required',
            'question_type' => 'required',
            'question_name' => 'required',
            'option_a' => 'required',
           'option_b' => 'required',
           'option_c' => 'required',
           'option_d' => 'required',
           'answer_option' => 'required'
            ),
            'categories' => Category::where('is_active',1)->get(),
            'tags' => Tag::where('is_active',1)->get(),
    );
}


/**
 * Function to get the Questions in front end
 * 
 * @return array
 */
public function getQuestions() {
    $optionsData = $this->question->where ( 'is_active',1 )->first()->toArray();
    $optionsCollection = $this->question->where ( 'is_active',1 )->get()->toArray();
    $optionsData['options'] = json_decode($optionsData['options']);
    
    return ['data' => [$optionsData],
            'examimgpath' => env('EXAM_IMAGE_PATH'),
    		'optionscollection' => $optionsCollection
    ];
}

public function getSingleQuestion() {
	$optionsData = $this->question->where ( 'id', $this->request->id )->where ( 'is_active',1 )->first ();
	$options = json_decode($optionsData->options);	
}

/**
 * Function to submit the exam results
 * 
 * @return array
 */
public function examSubmit() {	
	$user = Auth::user();
	app('log')->info(print_r($user,1));
	
	/*	
	$data = $this->request->all();
	$examAnswer = $this->examAnswer;
	$examAnswer->answers = json_encode($data);
	
	$examAnswer->user_id = 2;
	if($examAnswer->save()) {
		return true;
	} */
}
}

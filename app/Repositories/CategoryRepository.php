<?php

/**
 * Category Repository 
 *
 * To manage category related actions.
 *
 * @name       C2Theme
 * @version    1.0
 * @author     C2Theme Team <developers@c2theme.com>
 * @copyright  Copyright (C) 2016 Contus. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */

namespace Apptha\Repositories;
use Illuminate\Database\QueryException;
use Apptha\Models\Category;
use Contus\Base\Repositories\Repository;
use Contus\Base\Exceptions\InvalidRequestException;

class CategoryRepository extends Repository {
/**
 * Class initializer
 *
 * @return void
 */
public function __construct(Category $category) {
 parent::__construct ();
 $this->category = $category;
}
/**
 * This method is use to save the data in email templates tables
 *
 * @see \\Contus\Base\Contracts\ResourceInterface::store()
 *
 * @return boolean
 */
public function store() {
 return $this->addOrUpdate ( $this->request->all () );
}

/**
 * This method is use to update the email templates
 *
 * @see \Contus\Base\Contracts\ResourceInterface::update()
 * @return boolean
 */
public function update() {
 return $this->addOrUpdate ( $this->request->all (), $this->request->id );
}

/**
 * This method is use as a common method for both store and update email templates 
 *
 * @param array $requestData         
 * @param int $id         
 * @return boolean
 */
public function addOrUpdate($requestData, $id = null) {
  $operationStatus=true;
   if (! empty ( $id )) {
     $category = $this->category->find ( $id );
     $this->setRule ( 'name', 'required|unique:category,name,' . $category->id . '|max:50' );
    } else {
    	$category = $this->category;
     $this->setRule ( 'name', 'required|unique:category,name|max:50' );
    }
    $this->setRule ( 'is_active', 'required|numeric' );
    $this->_validate ();
    $category->fill ( $this->request->all() );  
    $category->fill ( array(
    		                    'creator_id' => 1,
    		                    'updator_id' => 1
    		             ) );
    if(empty($id)) {
    	$category->slug = str_slug($this->request->name);
    }
    $category->save ();
    return $operationStatus;
}
/**
 * Prepare the grid
 * set the grid model and relation model to be loaded
 *
 * @return \Contus\Base\Repositories\Repository
 */
public function prepareGrid() {
 $this->setGridModel ( $this->category );
 return $this;
}

/**
 * Update grid records collection query
 *
 * @param mixed $pushNotification         
 * @return mixed
 */
protected function updateGridQuery($category) {
 /**
  * updated the grid query by using this function and apply the is_active condition.
  */
 $filters = $this->request->input('filters');
 if (! empty ( $filters )) {
  foreach ( $filters as $key => $value ) {
   switch ($key) {
    case 'name' :
     $category->where ( 'name', 'like', '%' . $value . '%' )->get ();
     break;
    default :
     $category->where ( 'is_active', 1 )->orWhere('is_active', 0);
     break;
   }
  }
 }
 return $category;
}

/**
 * This method is use to soft delete the records
 *
 * @see \Contus\Base\Contracts\ResourceInterface::destroy()
 *
 * @return bool
 */
public function destroy() {
return $this->category->where ( 'id', $this->request->id )->update ( array (
  'is_active' => 0 
 ) );
}
/**
 * Method to get the data of single record of email template
 *
 * @see \Contus\Base\Contracts\ResourceInterface::edit()
 * @return array, id, list of email
 */
public function edit($id) {
 return array (
   'id' => $id,
   'categorySingleInfo' => $this->category->where ( 'id', $id )->first (),
   'rules' => array (
     'name' => 'required'
   )
 );
}
/**
 * Method to get the rules of email template
 *
 * @see \Contus\Base\Contracts\ResourceInterface::create()
 * @return array
 */
public function create() {
 return array (
   'rules' => array (
     'name' => 'required'
   )
 );
}
}

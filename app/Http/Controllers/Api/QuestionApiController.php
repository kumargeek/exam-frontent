<?php

/**
 * TagApiController
 *
 * To tag related activities
 *
 * @name       C2Theme
 * @version    1.0
 * @author     Kumar Ramalingam <developers@c2theme.con>
 * @copyright  Copyright (C) 2016 Contus. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Apptha\Http\Controllers\Api;

use Apptha\Repositories\QuestionRepository;
use Contus\Base\Controllers\Api\ApiController;

class QuestionApiController extends ApiController {

/**
 * Class initializer
 * 
 * @param TagRepository $tagRepository
 */
public function __construct(QuestionRepository $questionRepository) {
    parent::__construct ();
    $this->repository = $questionRepository;
}

/*Update Gallery Image*/
public function uploadfile() {
	$response=$this->repository->uploadFile();
	return ($response['status']) ? $this->getSuccessJsonResponse ([
	],  $response['filename'] ) : $this->getErrorJsonResponse ( [ ], $response['code'] );
}

public function uploadfilea() {
	$response=$this->repository->uploadFileA();
	return ($response['status']) ? $this->getSuccessJsonResponse ([
	],  $response['filename'] ) : $this->getErrorJsonResponse ( [ ], $response['code'] );
}

public function uploadfileb() {
	$response=$this->repository->uploadFileB();
	return ($response['status']) ? $this->getSuccessJsonResponse ([
	],  $response['filename'] ) : $this->getErrorJsonResponse ( [ ], $response['code'] );
}

public function uploadfilec() {
	$response=$this->repository->uploadFileC();
	return ($response['status']) ? $this->getSuccessJsonResponse ([
	],  $response['filename'] ) : $this->getErrorJsonResponse ( [ ], $response['code'] );
}

public function uploadfiled() {
	$response=$this->repository->uploadFileD();
	return ($response['status']) ? $this->getSuccessJsonResponse ([
	],  $response['filename'] ) : $this->getErrorJsonResponse ( [ ], $response['code'] );
}

public function editQuestion() {
	$response = $this->repository->editQuestion();
	return $this->getSuccessJsonResponse(['data' => $this->repository->editQuestion ()]);	
}

public function getQuestion() {
	$response = $this->repository->getQuestions();
	return $this->getSuccessJsonResponse(['data' => $this->repository->getQuestions ()]);
}

public function examSubmit() {
	$response = $this->repository->examSubmit();
	return $this->getSuccessJsonResponse(['data' => $response,'message' => 'Exam Submitted Successfully']);
}

public function getSingleQuestion() {
	return $this->getSuccessJsonResponse(['data' => $this->repository->getSingleQuestion ()]);
}

}
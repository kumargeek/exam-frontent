<?php

/**
 * CategoryApiController
 *
 * To category related activities
 *
 * @name       C2Theme
 * @version    1.0
 * @author     Kumar Ramalingam <developers@c2theme.con>
 * @copyright  Copyright (C) 2016 Contus. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Apptha\Http\Controllers\Api;

use Apptha\Repositories\CategoryRepository;
use Contus\Base\Controllers\Api\ApiController;

class CategoryApiController extends ApiController {

/**
 * Class initializer
 * 
 * @param CategoryRepository $categoryRepository
 */
public function __construct(CategoryRepository $categoryRepository) {
    parent::__construct ();
    $this->repository = $categoryRepository;
}
}
<?php

/**
 * TagApiController
 *
 * To tag related activities
 *
 * @name       C2Theme
 * @version    1.0
 * @author     Kumar Ramalingam <developers@c2theme.con>
 * @copyright  Copyright (C) 2016 Contus. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Apptha\Http\Controllers\Api;

use Apptha\Repositories\TagRepository;
use Contus\Base\Controllers\Api\ApiController;

class TagApiController extends ApiController {

/**
 * Class initializer
 * 
 * @param TagRepository $tagRepository
 */
public function __construct(TagRepository $tagRepository) {
    parent::__construct ();
    $this->repository = $tagRepository;
}
}
<?php
/**
 * AdminApiController
 *
 * To manage admin user related activities
 *
 * @name       MoverBee
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2016 Contus. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Apptha\Http\Controllers\Api;

use Apptha\Repositories\StudentRepository as StudentBaseRepository;
use Apptha\Repositories\Web\StudentRepository ;
use Contus\Base\Controllers\Api\ApiController;

class StudentApiController extends ApiController {
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
    parent::__construct ();

    if ($this->isMobileRequest()) {
      $this->repository = new StudentBaseRepository();
    }else{
      $this->repository = new StudentRepository();
    }
  }
}
